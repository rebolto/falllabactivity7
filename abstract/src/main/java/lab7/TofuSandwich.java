/**
 * @author Noah Diplarakis
 * @author Danny Hoang
 * @version 2023-10-18
 */
package lab7;
public class TofuSandwich extends VegetarianSandwich {
    /**
     * Creates a new TofuSandwich with tofu as the filling.
     */
    public TofuSandwich() {
        super("Tofu");
    }

    /**
     * Get the protein used in this tofu sandwich, which is the same as the filling.
     *
     * @returns {string} The protein, which is "Tofu".
     */
    @Override
    public String getProtein() {
        return super.getFilling();
    }
}
