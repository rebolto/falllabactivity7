/**
 * @extends {VegetarianSandwich}
 * @author Noah Diplarakis
 * @author Danny Hoang
 * @version 2023-10-18
 */
package lab7;
public class CaesarSandwich extends VegetarianSandwich {

    /**
     * Creates a new CaesarSandwich with Caesar dressing as the filling.
     */
    public CaesarSandwich() {
        super("Caesar dressing");
    }

    /**
     * Get the protein used in this Caesar sandwich.
     *
     * @returns {string} The protein, which is "Anchovies".
     */
    @Override
    public String getProtein() {
        return "Anchovies";
    }

    /**
     * Checks if the Caesar sandwich is vegan.
     *
     * @returns {boolean} Always returns false since it contains anchovies.
     */
    @Override
    public boolean isVegan() {
        return false;
    }

    /**
     * Checks if the Caesar sandwich is vegetarian.
     *
     * @returns {boolean} Always returns false since it contains anchovies.
     */
    public boolean isVegetarian() {
        return false;
    }
}
