/**

 * @author Noah Diplarakis
 * @author Danny Hoang
 * @version 2023-10-18
 */

package lab7;

/**
 * This class represents a Bagel Sandwich and implements the ISandwich interface.
 */
public class BagelSandwich implements ISandwhich {
    private String filling;

    /**
     * Constructs a BagelSandwich object with an empty filling.
     */
    public BagelSandwich() {
        this.filling = "";
    }

    /**
     * Gets the filling of the Bagel Sandwich.
     * @return The filling of the Bagel Sandwich.
     */
    public String getFilling() {
        return this.filling;
    }

    /**
     * Adds a topping to the Bagel Sandwich's filling.
     * @param topping The topping to be added.
     */
    public void addFilling(String topping) {
        this.filling += this.filling + ", " + topping;
    }

    /**
     * Determines whether the Bagel Sandwich is vegetarian.
     * @return This method always throws an UnsupportedOperationException as it's not implemented.
     * @throws UnsupportedOperationException Always thrown to indicate that the operation is not supported.
     */
    public boolean isVegetarian() {
        throw new UnsupportedOperationException();
    }
}