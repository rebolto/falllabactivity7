
/*
 * 
 * @author Noah Diplarakis
 * @author Danny Hoang
 * @version 2023-10-18
 */
package lab7;

public abstract class VegetarianSandwich implements ISandwhich {

    private String filling;

    /*
     * Constructor setting filling to ""
     */
    public VegetarianSandwich() {
        this.filling = "";
    }

    /*
     * Constructor to set filling to a set String
     */
    public VegetarianSandwich(String string) {
        this.filling = string;
    }

    @Override
    /*
     * Method to add filling to a sandwhich
     * 
     * @param{String}
     */
    public void addFilling(String topping) {
        String[] meats = { "chicken", "beef", "fish", "meat", "porc" };

        for (String meat : meats) {
            if (topping == meat) {
                throw new IllegalArgumentException("Value is not vegetarian friendly!");
            }
        }
        this.filling += this.filling + ", " + topping;
    }

    @Override
    /*
     * Checking if sandwich is vegetarian
     */
    public boolean isVegetarian() {
        return true;
    }
    /*
     * Checking if sandwich is Vegan
     */
    public boolean isVegan() {
        String[] fillings = this.filling.split(", ");

        for (String oneFilling : fillings) {
            if (oneFilling.equals("egg") || oneFilling.equals("cheese")) {
                return false;
            }
        }

        return true;
    }

    @Override
    /*
     * Getting Filling of the sandwhich
     */
    public String getFilling() {
        return this.filling;
    }
    /*
     * abstract method for derived class to implement.
     */
    public abstract String getProtein();
}
