/*
 * @author Noah Diplarakis
 * @author Danny Hoang
 * @date 2023-10-18
 */
package lab7;

public interface ISandwhich {
    /*
     * Used for derived class to implement this method 
     */
    public String getFilling();
    /*
     * Used for derived class to implement this method 
     */
    public void addFilling(String topping);
    /*
     *Used for derived class to implement this method 
     */
    public boolean isVegetarian();
}
