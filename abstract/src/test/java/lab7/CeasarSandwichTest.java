package lab7;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class CeasarSandwichTest {
    @Test
    public void testConstructor() {

        VegetarianSandwich vegesandwich = new CaesarSandwich();
        assertEquals("Caeser dressing", vegesandwich.getFilling());
    }

    @Test
    public void testgetProtien() {

        VegetarianSandwich vegesandwich = new CaesarSandwich();
        assertEquals("Anchovies", vegesandwich.getProtein());
    }

    @Test
    public void testisVegan() {
        VegetarianSandwich vegesandwich = new CaesarSandwich();
        assertFalse(vegesandwich.isVegan());
    }
}
