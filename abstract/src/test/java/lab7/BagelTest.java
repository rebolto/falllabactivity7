package lab7;

import org.junit.Test;
import static org.junit.Assert.*;

public class BagelTest {
    @Test
    public void testingConstructor() {
        BagelSandwich sandwich = new BagelSandwich();

    }

    @Test
    public void testGetFilling() {
        BagelSandwich s = new BagelSandwich();
        assertEquals("", s.getFilling());

    }

    @Test
    public void testAddFilling() {
        BagelSandwich s = new BagelSandwich();
        s.addFilling("chicken");
    }

    @Test
    public void testIsVegetarian() {

        try {
            BagelSandwich s = new BagelSandwich();
            assertTrue(s.isVegetarian());
            fail("was suppose to catch the exception");
        } catch (UnsupportedOperationException e) {
            System.out.println("catched");
        }
    }
}
